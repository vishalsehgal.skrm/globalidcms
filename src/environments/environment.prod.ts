export const environment = {
  production: true,
  apiUrl: 'https://preprod.skrm.co.in/cms-new-api-v3',
  appVersion : 'PRODCMS2.0',
  TOKENDATA_KEY : 'THISKEYISUSEDTOSAVETOKENDETAILS'
};
