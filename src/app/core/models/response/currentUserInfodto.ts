export class CurrentUserInfoDto{
    userId : number;
    userFullName : string;
    userEmail : string
}