export class LoginResponseDto{
    
    token: string;
    expiringIn: number;
    refreshToken : string;
    userId: number;

    setAuth(auth: LoginResponseDto) {
        this.token = auth.token;
        this.refreshToken = auth.refreshToken;
        this.expiringIn = auth.expiringIn;
        this.userId = auth.userId;
      }
}