import { Injectable } from "@angular/core";
import {
    HttpInterceptor, HttpHandler, HttpRequest,
} from '@angular/common/http';
import { Router } from "@angular/router";
import { AuthService } from "src/app/modules/auth/services/auth.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { LoginResponseDto } from "../models/response/loginresponsedto";
import { BehaviorSubject, catchError, filter, lastValueFrom, Observable, of, switchMap, throwError, take } from "rxjs";
import { environment } from "src/environments/environment";
import { ApiResponse } from "../models/response/apiresponse";
 
import Swal from 'sweetalert2';
@Injectable()
export class JWTTokenInterceptor implements HttpInterceptor {

    private authLocalStorageToken = `${environment.appVersion}-${environment.TOKENDATA_KEY}`;

    constructor(private router: Router, private authService: AuthService, private modal: NgbModal) { }

    intercept(request: HttpRequest<any>, next: HttpHandler) {

        request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });

        var response = this.authService.getAuthFromLocalStorage();
        if (response) {
            let tokenInfo: LoginResponseDto = response.data;
            if (tokenInfo) {
                if (tokenInfo.token) {
                    request = this.addTokenHeader(request, tokenInfo.token);
                }
            }
        }

        return next.handle(request).pipe(catchError(errorData => {
            if (errorData.status === 401) {
                return this.handle401Error(request, next);
            }
            else if (errorData.status === 500) {
                this.modal.dismissAll('Unauthorized');
                this.router.navigate(['/error/error-500']);
            }
            else if (errorData.status === 404) {
                this.modal.dismissAll('Unauthorized');
                this.router.navigate(['/error/error-404']);
            }
            else if(errorData.status === 412 || errorData.status === 400) {
                 

                this.warningNotification(errorData.status, errorData.error.message);
            }
            
            return throwError(errorData);
        }));
    }

    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);

            var currentresponse = this.authService.getAuthFromLocalStorage();

            if (currentresponse) {
                let currentTokenInfo: LoginResponseDto = currentresponse.data;
                if (currentTokenInfo) {

                    if (currentTokenInfo.token) {
                        return this.authService.refreshToken(currentTokenInfo.userId, currentTokenInfo.refreshToken).pipe(
                            switchMap((token: ApiResponse<LoginResponseDto>) => {
                                this.isRefreshing = false;
                                this.refreshTokenSubject.next(token);
                               
                                return next.handle(this.addTokenHeader(request, token.data.token));
                            }),
                            catchError((err) => {
                                this.isRefreshing = false;

                                this.authService.logout();
                                return throwError(err);
                            })
                        );
                    }
                }
            }
        }

        return this.refreshTokenSubject.pipe(
            filter(token => token !== null),
            take(1),
            switchMap((token) => 
            next.handle(this.addTokenHeader(request, token.data.token)))
        );
    }

    private addTokenHeader(request: HttpRequest<any>, token: string) {
        /* for Spring Boot back-end */
        // return request.clone({ headers: request.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token) });
        /* for Node.js Express back-end */

        return request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
    }

    warningNotification(status: any, message: string) {
        Swal.fire({
          title: status === 412 ? 'Precondition Failed!' : 'Bad Request',
          text: message,
          icon: 'warning'
        }).then(async (result) => {
           
        });
      }
}