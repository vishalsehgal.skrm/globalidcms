import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { lastValueFrom } from 'rxjs';
import { AuthService } from '../../modules/auth/services/auth.service';
import { LoginResponseDto } from 'src/app/core/models/response/loginresponsedto';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  public jwtHelper: JwtHelperService = new JwtHelperService();

  constructor(private router: Router, private authService: AuthService) { }

  async canActivate() {
    var response = this.authService.getAuthFromLocalStorage();

    //console.log(response);

    if (response) {
      let tokenInfo: LoginResponseDto = response.data;

      if (tokenInfo) {
        if (!tokenInfo.token || !tokenInfo.refreshToken) {
          return false;
        }

        if (tokenInfo.token && !this.jwtHelper.isTokenExpired(tokenInfo.token)) {
          return true;
        }
      }
    }
    this.authService.logout();
    return false;
  }


  private async refreshingTokens(currentUserId: number | 0, refreshToken: string | null): Promise<boolean> {

    if (!currentUserId || !refreshToken) {
      return false;
    }

    let isRefreshSuccess: boolean;
    try {

      const response = await lastValueFrom(this.authService.refreshToken(currentUserId, refreshToken));
      isRefreshSuccess = response.success;
    }
    catch (ex) {
      isRefreshSuccess = false;
    }
    return isRefreshSuccess;
  }
}
