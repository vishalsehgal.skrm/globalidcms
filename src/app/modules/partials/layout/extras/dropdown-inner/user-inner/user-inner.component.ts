import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiResponse } from 'src/app/core/models/response/apiresponse';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { TranslationService } from 'src/app/modules/i18n';
import { first } from 'rxjs/operators';

import { LoginResponseDto } from 'src/app/core/models/response/loginresponsedto';
import { CurrentUserInfoDto } from 'src/app/core/models/response/currentUserInfodto';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-user-inner',
  templateUrl: './user-inner.component.html',
})
export class UserInnerComponent implements OnInit, OnDestroy {
  @HostBinding('class')
  class = `menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px`;
  @HostBinding('attr.data-kt-menu') dataKtMenu = 'true';

  language: LanguageFlag;
  userInfo: CurrentUserInfoDto;

  langs = languages;
  private unsubscribe: Subscription[] = [];

  constructor(
    private authService: AuthService,
    private translationService: TranslationService
  ) { }

  ngOnInit(): void {
    this.userInfo = this.authService.getcurrentUserInfoFromToken();
    this.setLanguage(this.translationService.getSelectedLanguage());
  }

  logoutWarningNotification(status: any, message: string) {
    Swal.fire({
      title: 'Are you sure to logout?',
      text: 'This process is irreversible.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, go ahead.',
      cancelButtonText: 'No, let me think',
    }).then(async (result) => {

      if (result.value) {

        await this.logout();

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        
      }

    });
  }

  logout() {

    const currentUser = this.authService.currentUserValue;

    if (currentUser) {

      let tokenData: LoginResponseDto | null = currentUser.data;
      if (tokenData) {

        let refreshToken: string | null = tokenData.refreshToken;
        let userId: number | 0 = tokenData.userId;

        const Subscr = this.authService
          .blacklistRefreshToken(userId, refreshToken)
          .pipe(first())
          .subscribe((response: ApiResponse<LoginResponseDto> | null) => {
            if (response && response.success) {

              this.authService.logout();
             window.location.reload();
            }
          }
          );
        this.unsubscribe.push(Subscr);
      }

      // logged in so return true
      // return true;
    }
  }

  setLanguage(lang: string) {
    this.langs.forEach((language: LanguageFlag) => {
      if (language.lang === lang) {
        language.active = true;
        this.language = language;
      } else {
        language.active = false;
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }
}

interface LanguageFlag {
  lang: string;
  name: string;
  flag: string;
  active?: boolean;
}

const languages = [
  {
    lang: 'en',
    name: 'English',
    flag: './assets/media/flags/united-states.svg',
  },
  {
    lang: 'zh',
    name: 'Mandarin',
    flag: './assets/media/flags/china.svg',
  },
  {
    lang: 'es',
    name: 'Spanish',
    flag: './assets/media/flags/spain.svg',
  },
  {
    lang: 'ja',
    name: 'Japanese',
    flag: './assets/media/flags/japan.svg',
  },
  {
    lang: 'de',
    name: 'German',
    flag: './assets/media/flags/germany.svg',
  },
  {
    lang: 'fr',
    name: 'French',
    flag: './assets/media/flags/france.svg',
  },
];
