import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ApiResponse } from '../../../core/models/response/apiresponse';
import { LoginResponseDto } from 'src/app/core/models/response/loginresponsedto';
 
@Injectable({
  providedIn: 'root',
})

export class AuthHTTPService {
  constructor(private http: HttpClient) {}

  // public methods
  login(username: string, password: string): Observable<ApiResponse<LoginResponseDto>> {
    return this.http.post<ApiResponse<LoginResponseDto>>(`${environment.apiUrl}/api/v1/auth`, {
        username,
      password,
    });
  }

  refreshToken(userId: number, refreshToken: string): Observable<ApiResponse<LoginResponseDto>> {
    return this.http.post<ApiResponse<LoginResponseDto>>(`${environment.apiUrl}/api/v1/refreshtoken`, {
        userId,
      refreshToken,
    });
  }

  logout(userId : number, refreshToken: string): Observable<ApiResponse<LoginResponseDto>> {
    return this.http.post<ApiResponse<LoginResponseDto>>(`${environment.apiUrl}/api/v1/auth/logout`, {
        userId,
      refreshToken
    });
  }
 
  // Your server should check email => If email exists send link to the user and return true | If email doesn't exist return false
  forgotPassword(email: string): Observable<ApiResponse<LoginResponseDto>> {
    return this.http.post<ApiResponse<LoginResponseDto>>(`${environment.apiUrl}/api/v1/password/forgot`, {
      email,
    });
  }
 
}
