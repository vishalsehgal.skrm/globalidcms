import { Injectable, OnDestroy } from '@angular/core';
import { ApiResponse } from '../../../core/models/response/apiresponse';
import { Observable, BehaviorSubject, of, Subscription } from 'rxjs';
import { map, catchError, switchMap, finalize } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

import { AuthHTTPService } from './auth-http.service';
import { LoginResponseDto } from 'src/app/core/models/response/loginresponsedto';
import { CurrentUserInfoDto } from 'src/app/core/models/response/currentUserInfodto';

@Injectable({ providedIn: 'root' })

export class AuthService implements OnDestroy {

  private unsubscribe: Subscription[] = [];
  private authLocalStorageToken = `${environment.appVersion}-${environment.TOKENDATA_KEY}`;

  currentUser$: Observable<ApiResponse<LoginResponseDto>>;
  isLoading$: Observable<boolean>;
  currentUserSubject$: BehaviorSubject<ApiResponse<LoginResponseDto>>;
  isLoadingSubject$: BehaviorSubject<boolean>;

  get currentUserValue(): ApiResponse<LoginResponseDto> {
    return this.currentUserSubject$.value;
  }

  set currentUserValue(user: ApiResponse<LoginResponseDto>) {
    this.currentUserSubject$.next(user);
  }

  constructor(
    private router: Router,
    private authHttpService: AuthHTTPService
  ) {
    this.isLoadingSubject$ = new BehaviorSubject<boolean>(false);
    this.currentUserSubject$ = new BehaviorSubject<ApiResponse<LoginResponseDto>>(undefined);
    this.currentUser$ = this.currentUserSubject$.asObservable();
    this.isLoading$ = this.isLoadingSubject$.asObservable();
    const subscr = this.getCurrentToken().subscribe();

    this.unsubscribe.push(subscr);
  }

  getcurrentUserInfoFromToken(): CurrentUserInfoDto {

    var apiResponseFromToken = this.currentUserValue;

    if (apiResponseFromToken) {
      let tokenData: LoginResponseDto | null = apiResponseFromToken.data;

      if (tokenData) {
        let token: string | null = tokenData.token;
        if (token) {


          let decodedJWT = JSON.parse(window.atob(token.split('.')[1]));

          if (decodedJWT) {
 
            const currentUser = new CurrentUserInfoDto();
 
            currentUser.userId = decodedJWT.actort,
            currentUser.userEmail = decodedJWT.email,
            currentUser.userFullName = decodedJWT.unique_name

            return currentUser;
          }
        }
      }
    }

    return undefined;
  }
  
  login(username: string, password: string): Observable<ApiResponse<LoginResponseDto>> {

    this.isLoadingSubject$.next(true);
    return this.authHttpService.login(username, password).pipe(
      map((auth: ApiResponse<LoginResponseDto>) => {
        this.setAuthFromLocalStorage(auth);
        this.currentUserSubject$.next(auth);
        return auth;
      }),
      switchMap(() => this.getCurrentToken()),
      catchError((err) => {
        this.isLoadingSubject$.next(false);
        return of(err.error);
      }),
      finalize(() => this.isLoadingSubject$.next(false))
    );
  }

  refreshToken(userId: number, refreshToken: string): Observable<ApiResponse<LoginResponseDto>> {
    this.currentUserSubject$.next(null);
    this.isLoadingSubject$.next(true);
    return this.authHttpService.refreshToken(userId, refreshToken).pipe(
      map((auth: ApiResponse<LoginResponseDto>) => {
        this.currentUserSubject$.next(auth);
        this.setAuthFromLocalStorage(auth);
        return auth;
      }),
      switchMap(() => this.getCurrentToken()),
      catchError((err) => {
        this.isLoadingSubject$.next(false);
        return of(err.error);
      }),
      finalize(() => this.isLoadingSubject$.next(false))
    );
  }
 
  getCurrentToken(): Observable<ApiResponse<LoginResponseDto>> {
    //this.currentUserSubject$.next(null);
    const auth = this.getAuthFromLocalStorage();
    if (!auth) {
      return of(undefined);
    }
    else {

      if (!auth.success) {

        this.logout();
        return of(undefined);
      }
      else {
        this.isLoadingSubject$.next(true);
        this.currentUserSubject$.next(auth);
        return of(auth);
      }

    }
  }

  blacklistRefreshToken(userId: number, refreshToken: string): Observable<ApiResponse<LoginResponseDto>> {
    return this.authHttpService.logout(userId, refreshToken).pipe(
      map((auth: ApiResponse<LoginResponseDto>) => {
        return auth;
      }),
      catchError((err) => {
        return of(err.error);
      })
    );
  }

  logout() {
    this.currentUserSubject$.next(null);
    localStorage.removeItem(this.authLocalStorageToken);
    this.router.navigate(['/auth/login'], {
      queryParams: {},
    });
  }

  // private methods
  private setAuthFromLocalStorage(response: ApiResponse<LoginResponseDto>): boolean {
    // store auth authToken/refreshToken/epiresIn in local storage to keep user logged in between page refreshes
    if (response) {
      localStorage.setItem(this.authLocalStorageToken, JSON.stringify(response));
      return true;
    }
    return false;
  }

  public getAuthFromLocalStorage(): ApiResponse<LoginResponseDto> | undefined {
    try {
      const lsValue = localStorage.getItem(this.authLocalStorageToken);

      if (!lsValue) {
        return undefined;
      }

      const authData = JSON.parse(lsValue);
      return authData;
    } catch (error) {

      return undefined;
    }
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }
}