import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './components/login/login.component';
import { ForgotpasswordComponent } from './components/forgot-password/forgot-password.component';
 
import { AuthRoutingModule } from './auth-routing.module';
 
@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    ForgotpasswordComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthRoutingModule
  ],
  exports: [RouterModule]
})
export class AuthModule { }
