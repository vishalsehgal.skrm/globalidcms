import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorsComponent } from './errors.component';
import { InternalServerErrorComponent } from './internal-server-error/internal-server-error.component';
import { NotFoundErrorComponent } from './not-found-error/not-found-error.component';
// import { Error3Component } from './error3/error3.component';
// import { Error4Component } from './error4/error4.component';
// import { Error5Component } from './error5/error5.component';

const routes: Routes = [
  {
    path: '',
    component: ErrorsComponent,
    children: [
      {
        path: 'error-500',
        component: InternalServerErrorComponent,
      },
      {
        path: 'error-404',
        component: NotFoundErrorComponent,
      },
    //   {
    //     path: 'error-3',
    //     component: Error3Component,
    //   },
    //   {
    //     path: 'error-4',
    //     component: Error4Component,
    //   },
    //   {
    //     path: 'error-5',
    //     component: Error5Component,
    //   },
      { path: '', redirectTo: 'error-500', pathMatch: 'full' },
      {
        path: '**',
        component: InternalServerErrorComponent,
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ErrorsRoutingModule {}
