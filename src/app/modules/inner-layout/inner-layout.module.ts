import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg-2';
import { RouterModule, Routes } from '@angular/router';

import {NgbDropdownModule, NgbProgressbarModule,NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { TranslationModule } from '../../modules/i18n';

import { InnerLayoutComponent } from './inner-layout.component';

import { ExtrasModule } from '../partials/layout/extras/extras.module';

import { AsideComponent } from './components/shared/aside/aside.component';
import { ContentComponent } from './components/shared/content/content.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { ToolbarComponent } from './components/shared/toolbar/toolbar.component';
import { TopbarComponent } from './components/shared/topbar/topbar.component';
import { AsideMenuComponent } from './components/shared/aside/aside-menu/aside-menu.component';
import { ScriptsInitComponent } from './components/shared/scripts-init/scripts-init.component';
 
import { PageTitleComponent } from './components/shared/header/page-title/page-title.component';
import { HeaderMenuComponent } from './components/shared/header/header-menu/header-menu.component';
import { DrawersModule, DropdownMenusModule, ModalsModule, EngagesModule} from '../partials';
import {EngagesComponent} from "../partials/layout/engages/engages.component";
 
import { InnerLayoutRouting } from 'src/app/route-configuration/inner-layout-routing';
import { FormsModule } from '@angular/forms';
 
const routes: Routes = [
  {
    path: '',
    component: InnerLayoutComponent,
    children: InnerLayoutRouting,
  },
];


@NgModule({
  declarations: [
    InnerLayoutComponent,
    AsideComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    ScriptsInitComponent,
    ToolbarComponent,
    AsideMenuComponent,
    TopbarComponent,
    PageTitleComponent,
    HeaderMenuComponent,
    EngagesComponent
    
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    TranslationModule,
    InlineSVGModule,
    NgbDropdownModule,
    NgbProgressbarModule,
    ExtrasModule,
    ModalsModule,
    DrawersModule,
    EngagesModule,
    DropdownMenusModule,
    NgbTooltipModule,
    TranslateModule
  ],
  exports: [RouterModule],
  providers: [

  ]
})
export class InnerLayoutModule { }
