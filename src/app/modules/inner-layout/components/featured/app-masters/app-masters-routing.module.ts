import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppMastersComponent } from './app-masters.component';
import { CountryMasterComponent } from './components/country-master/country-master.component';
import { StateMasterComponent } from './components/state-master/state-master.component';
 

const routes: Routes = [
  {
    path: '',
    component: AppMastersComponent,
    children: [
      {
        path: 'countries',
        component: CountryMasterComponent,
      },
      {
        path: 'states',
        component: StateMasterComponent,
      },
      // {
      //   path: 'districts',
      //   component: DistrictMasterComponent,
      // },
      // {
      //   path: 'cities',
      //   component: CityMasterComponent,
      // },
      { path: '', redirectTo: 'countries', pathMatch: 'full' },
      { path: '**', redirectTo: 'countries', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppMastersRoutingModule { }
