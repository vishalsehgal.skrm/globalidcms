export interface IPagination<T>{
    totalRecordCount: number,
    pageIndex: number,
    pageSize: number,
    list: T[]
}