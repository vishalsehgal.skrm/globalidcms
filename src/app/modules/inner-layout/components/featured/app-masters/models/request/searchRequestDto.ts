export interface ISearchRequestDto {
    pageIndex: number | null;
    pageSize: number | null;
    orderBy: string | null;
    orderByDirection: string | null;
}