import { ISearchRequestDto } from "../../../models/request/searchRequestDto";

export interface ICountrySearchRequestDto extends ISearchRequestDto {
    name: string | null;
    isoCode: string | null;
    isdCode: string | null;
    pageIndex: number | null;
    pageSize: number | null;
    orderBy: string | null;
    orderByDirection: string | null;
}