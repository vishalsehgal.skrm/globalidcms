export interface ICreateCountry {
    name: string;
    isoCode: string;
    isdCode: string;
  }
