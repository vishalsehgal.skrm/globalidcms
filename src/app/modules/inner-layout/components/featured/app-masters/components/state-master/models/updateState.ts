export interface IUpdateState {
    id: number;
    name: string;
    countryId: number;
  }
