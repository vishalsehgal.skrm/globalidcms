export interface IUpdateCountry {
    id: number;
    name: string;
    isoCode: string;
    isdCode: string;
  }