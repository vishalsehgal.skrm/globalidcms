import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/core/models/response/apiresponse';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';
import { Router } from '@angular/router';

import { CountryMastersHTTPService } from './http-service/country-masters-http.service';
import { ICountrySearchRequestDto } from '../models/countrySearchRequestDto';
import { ICountryDto } from '../models/countryResponseDto';
import { IPagination } from '../../../models/response/pagination';

const DEFAULT_COUNTRY: ICountrySearchRequestDto = {
name : '',
isdCode : '',
isoCode : '',
orderBy : 'Id',
orderByDirection : 'Desc',
pageIndex : 0,
pageSize : 10
};

@Injectable({ providedIn: 'root' })

export class GetCountryListService {

  // Private fields
  private _items$ = new BehaviorSubject<ICountryDto[]>([]);
  
  private _isLoading$ = new BehaviorSubject<boolean>(false);
  private _isFirstLoading$ = new BehaviorSubject<boolean>(true);
  private _tableState$ = new BehaviorSubject<ICountrySearchRequestDto>(DEFAULT_COUNTRY);
  private _errorMessage$ = new BehaviorSubject<string>('');
  private _successMessage$ = new BehaviorSubject<string>('');
  
  private _subscriptions$: Subscription[] = [];
  private _isSuccess$ = new BehaviorSubject<boolean>(false);

  private _totalItems$ = new BehaviorSubject<number>(0);
  private _pageIndex$ = new BehaviorSubject<number>(0);
  private _pageSize$ = new BehaviorSubject<number>(0);

  // Getters
  get items$() {
    return this._items$.asObservable();
  }
 
 
  get totalItems$() {
    return this._totalItems$.asObservable();
  }

  get pageIndex$() {
    return this._pageIndex$.asObservable();
  }

  get pageSize$() {
    return this._pageSize$.asObservable();
  }

  get isLoading$() {
    return this._isLoading$.asObservable();
  }
  get isFirstLoading$() {
    return this._isFirstLoading$.asObservable();
  }
  get errorMessage$() {
    return this._errorMessage$.asObservable();
  }

  get successMessage$() {
    return this._successMessage$.asObservable();
  }

  get isSuccess$() {
    return this._isSuccess$.asObservable();
  }

  get subscriptions$() {
    return this._subscriptions$;
  }

 
  get sortingColumnName$() {
    return this._tableState$.value.orderBy;
  }

  get sortingDirection$() {
    return this._tableState$.value.orderByDirection;
  }

  constructor(
    private router: Router,
    private appMastersHTTPService: CountryMastersHTTPService
  ) {

  }
 
   // Base Methods
   public patchState(patch: Partial<ICountrySearchRequestDto>) {
    this.patchStateWithoutFetch(patch);
    this.getList(this._tableState$.value);
  }

  public patchStateWithoutFetch(patch: Partial<ICountrySearchRequestDto>) {
    const newState = Object.assign(this._tableState$.value, patch);
    this._tableState$.next(newState);
  }


  async getList(countrySearchRequestDto: ICountrySearchRequestDto) {

    this._isLoading$.next(true);
    this._errorMessage$.next('');
    
    const request = (await this.appMastersHTTPService.GetCountryListWithPagination(countrySearchRequestDto))
      .pipe(
        map(
          (response: ApiResponse<IPagination<ICountryDto>>) => {

            if (response) {
              if (response.success) {
                if (response.data) {
                  if (response.data.list) {
                    this._items$.next(response.data.list);
                    this._totalItems$.next(response.data.totalRecordCount);
                    this._pageIndex$.next(response.data.pageIndex);
                    this._pageSize$.next(response.data.pageSize);
                  }
                }
              }
              else {
                this._errorMessage$.next(response.message);
              }
            }
            else {
              this._errorMessage$.next('Something went wrong. Please try again later.')
            }
          }
        ),
        catchError((err) : any => {
          this._isSuccess$.next(false);
          this._errorMessage$.next(err.error.message);
          
        }),
        finalize(() => {
          this._isLoading$.next(false);
        })
      )
      .subscribe();

    this._subscriptions$.push(request);
  }
}