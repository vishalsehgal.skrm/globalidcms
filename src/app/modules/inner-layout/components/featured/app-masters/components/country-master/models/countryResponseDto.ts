export interface ICountryDto{
        id: number;
        name: string;
        isdCode: string;
        isoCode: string;
        createdBy: number;
        createdByName: string | '';
        createdDate: Date;
        modifiedBy: number | null;
        modifiedByName: string | '';
        modifiedDate: string | null;
        isActive: boolean;
        isReviewed: boolean;
        reviewedBy: number | null;
        reviewedByName: string | '';
        reviewedDate: string;
        isDeleted: boolean;
        deletedDate: string
}