import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ICountrySearchRequestDto } from './models/countrySearchRequestDto';
import { ICountryDto } from './models/countryResponseDto';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortState, ISortView } from 'src/app/modules/sort-icon/sort.model';
import { DatePipe } from '@angular/common';

import { AddCountryComponent } from './components/add-country/add-country.component';
import { EditCountryComponent } from './components/edit-country/edit-country.component';

import { GetCountryListService } from './services/get-country-list.service';
import { UpdateCountryReviewService } from './services/update-country-review.service';
import { UpdateCountryStatusService } from './services/update-country-status.service';
import { DeleteCountryService } from './services/delete-country.service';
 
import Swal from 'sweetalert2';

const EMPTY_COUNTRY_SEARCH: ICountrySearchRequestDto = {
  orderBy: 'Id',
  orderByDirection: 'Desc',
  name: '',
  isoCode: '',
  isdCode: '',
  pageIndex: 0,
  pageSize: 10
};

@Component({
  selector: 'app-country-master',
  templateUrl: './country-master.component.html',
  styleUrls: ['./country-master.component.scss'],
  providers: [
    GetCountryListService,
    UpdateCountryReviewService,
    UpdateCountryStatusService,
    DeleteCountryService
  ]
})

export class CountryMasterComponent implements OnInit,
  ISortView {

  searchform: FormGroup;
  pageIndex: number;
  pageSize: number;
  totalRecordCount: number;

  sorting: SortState;
  isLoading: boolean;
  hasError: boolean;
  errorMessage: string = '';
  countrySearchRequestDto: ICountrySearchRequestDto;

  // private fields
  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

  countryList: ICountryDto[];
  private subscriptions: Subscription[] = [];

  constructor(
    private datePipe: DatePipe,
    public getCountryListService: GetCountryListService,
    public updateCountryReviewService: UpdateCountryReviewService,
    public updateCountryStatusService: UpdateCountryStatusService,
    public deleteCountryService: DeleteCountryService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {
  }

  async ngOnInit() {

    this.countrySearchRequestDto = EMPTY_COUNTRY_SEARCH;
    this.loadForm();

    this.sorting = new SortState();
    // this.sorting.column = this.getCountryListService.sortingColumnName$;
    // this.sorting.direction = this.getCountryListService.sortingDirection$;

    await this.getCountryListService.getList(null);
    this.getCountryListService.pageIndex$.subscribe(x => this.pageIndex = x);
    this.getCountryListService.pageSize$.subscribe(x => this.pageSize = x);
    this.getCountryListService.totalItems$.subscribe(x => this.totalRecordCount = x);

    const sb = this.getCountryListService.isLoading$.subscribe(res => this.isLoading = res);
    this.subscriptions.push(sb);
  }


  loadForm() {
    this.searchform = this.fb.group({
      searchCountryName: [''],
      searchISO: [''],
      searchISD: ['']
    });
  }

  private prepareCountry() {
    const formData = this.searchform.value;
    this.countrySearchRequestDto.name = formData.searchCountryName;
    this.countrySearchRequestDto.isdCode = formData.searchISD;
    this.countrySearchRequestDto.isoCode = formData.searchISO;
    this.countrySearchRequestDto.pageIndex = this.pageIndex;

    this.countrySearchRequestDto.orderBy = this.sorting.column;
    this.countrySearchRequestDto.orderByDirection = this.sorting.direction;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }

  // sorting
  async sort(column: any) {
    const sorting = this.sorting;

    const isActiveColumn = sorting.column === column;
    if (!isActiveColumn) {
      sorting.column = column;
      sorting.direction = 'asc';
    } else {
      sorting.direction = sorting.direction === 'asc' ? 'desc' : 'asc';
    }

    this.prepareCountry();

    this.countrySearchRequestDto.orderBy = this.sorting.column;
    this.countrySearchRequestDto.orderByDirection = this.sorting.direction;

    await this.getCountryListService.getList(this.countrySearchRequestDto);
  }

  async create() {
    const modalRef = this.modalService.open(AddCountryComponent, { size: 'md' });
    modalRef.result.then(() =>
      this.successNotification('Country added successfully!')
    );
  }

  async edit(id: number) {

    const modalRef = this.modalService.open(EditCountryComponent, { size: 'md' });
    modalRef.componentInstance.id = id;
    modalRef.result.then(() =>
      this.successNotification('Country updated successfully!')
    );
  }

  async search() {

    this.prepareCountry();

    await this.getCountryListService.getList(this.countrySearchRequestDto);
  }

  async resetSearch() {
    this.pageIndex = 1;
    this.searchform.reset();

    this.sorting.column = 'Id';
    this.sorting.direction = 'Desc';

    await this.search();
  }


  async review(event: any) {

    const sb = this.updateCountryReviewService.isLoading$.subscribe(res => this.isLoading = res);

    await this.updateCountryReviewService.UpdateReview(event);

    this.subscriptions.push(sb);
  }

  async status(event: any) {

    const sb = this.updateCountryStatusService.isLoading$.subscribe(res => this.isLoading = res);

    await this.updateCountryStatusService.UpdateStatus(event);

    this.subscriptions.push(sb);
  }

  async delete(event: any) {
    const sb = this.deleteCountryService.isLoading$.subscribe(res => this.isLoading = res);

    await this.deleteCountryService.Delete(event);

    this.subscriptions.push(sb);
  }

  async onPageChanged(event: any) {

    this.pageIndex = event;
    this.prepareCountry();

    this.countrySearchRequestDto.pageIndex = event;

    if (event <= 1) {
      this.countrySearchRequestDto.orderBy = 'Id';
      this.countrySearchRequestDto.orderByDirection = 'Desc';
    }
    else {
      this.countrySearchRequestDto.orderBy = this.sorting.column;
      this.countrySearchRequestDto.orderByDirection = this.sorting.direction;
    }


    await this.getCountryListService.getList(this.countrySearchRequestDto);

  }

  successNotification(message: string) {
    Swal.fire({
      title: 'Success!',
      text: message,
      icon: 'success'
    }).then(async (result) => {
      await this.search();
    });
  }

  deleteConfirmation(id: any) {
    Swal.fire({
      title: 'Are you sure to delete?',
      text: 'This process is irreversible.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, go ahead.',
      cancelButtonText: 'No, let me think',
    }).then(async (result) => {
      if (result.value) {

        await this.delete(id);

        Swal.fire('Removed!', 'Country removed successfully.', 'success').then(async () => {
          await this.search();
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'Country still in our database.', 'error');
      }
    });
  }

  statusConfirmation(id: any, status: any) {
    Swal.fire({
      title: status ? 'Are you sure to deactivate?' : 'Are you sure to activate?',
      text: status ? 'This process will deactivate the record.' : 'This process will activate the record.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, go ahead.',
      cancelButtonText: 'No, let me think',
    }).then(async (result) => {
      if (result.value) {

        await this.status(id);

        Swal.fire(status ? 'Deactive!' : 'Activate!', status ? 'Country deactivated successfully.' : 'Country activated successfully.', 'success').then(async () => {
          await this.search();
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', status ? 'Country still activated.' : 'Country still deactivated.', 'error');
      }
    });
  }

  reviewConfirmation(id: any) {
    Swal.fire({
      title: 'Are you sure to review?',
      text: 'This process will mark the record as "Reviewed".',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, go ahead.',
      cancelButtonText: 'No, let me think',
    }).then(async (result) => {
      if (result.value) {

        await this.review(id);

        Swal.fire('Reviewed!', 'Country reviewed successfully.', 'success').then(async () => {
          await this.search();
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'Country review still pending.', 'error');
      }
    });
  }
}
