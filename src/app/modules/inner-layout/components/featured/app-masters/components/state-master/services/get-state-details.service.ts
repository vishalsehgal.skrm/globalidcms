import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/core/models/response/apiresponse';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { catchError, finalize, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { StateMastersHTTPService } from './http-service/state-masters-http.service';
import { IStateDto } from '../models/stateResponseDto';

@Injectable({ providedIn: 'root' })

export class GetStateService {

  // Private fields
  private _itemDetails$ = new BehaviorSubject<IStateDto>(null);

  private _isLoading$ = new BehaviorSubject<boolean>(false);
  private _errorMessage$ = new BehaviorSubject<string>('');
  private _successMessage$ = new BehaviorSubject<string>('');
  private _isSuccess$ = new BehaviorSubject<boolean>(false);
  private _subscriptions$: Subscription[] = [];

  //Observables
  get itemDetails$() {
    return this._itemDetails$.asObservable();
  }

  get isLoading$() {
    return this._isLoading$.asObservable();
  }

  get errorMessage$() {
    return this._errorMessage$.asObservable();
  }

  get successMessage$() {
    return this._successMessage$.asObservable();
  }

  get isSuccess$() {
    return this._isSuccess$.asObservable();
  }

  get subscriptions() {
    return this._subscriptions$;
  }

  //Observable Values
  get isLoadingValue$() {
    return this._isLoading$.value;
  }

  get errorMessageValue$() {
    return this._errorMessage$.value;
  }

  get successMessageValue$() {
    return this._successMessage$.value;
  }

  get isSuccessValue$() {
    return this._isSuccess$.value;
  }

  get itemDetailsValue$() {
    return this._itemDetails$.value;
  }
 

  constructor(
    private router: Router,
    private stateMastersHTTPService: StateMastersHTTPService
  ) {

  }

  async GetDetails(id: number) : Promise<Observable<IStateDto>>{

    this._isLoading$.next(true);
    this._errorMessage$.next('');
    this._successMessage$.next('');
    this._isSuccess$.next(false);

    const request = (await this.stateMastersHTTPService.GetDetails(id))
      .pipe(
        map(
          (response: ApiResponse<IStateDto>) => {

            if (response) {
              if (response.success) {
                if (response.data) {
                  this._itemDetails$.next(response.data);
                  this._isSuccess$.next(true);
                  this._successMessage$.next('State reviewed successfully.');
                }
                else {
                  this._isSuccess$.next(false);
                  this._errorMessage$.next(response.message);
                }
              }
              else {
                this._isSuccess$.next(false);
                this._errorMessage$.next(response.message);
              }
            }
            else {
              this._isSuccess$.next(false);
              this._errorMessage$.next('Something went wrong. Please try again later.')
            }
          }
        ),
        catchError((err): any => {
          this._isSuccess$.next(false);
          this._errorMessage$.next(err.error.message);
          return of(null);
        }),
        finalize(() => {
          this._isLoading$.next(false);
        })
      )
      .subscribe();

    this._subscriptions$.push(request);

    return this._itemDetails$;
  }


}