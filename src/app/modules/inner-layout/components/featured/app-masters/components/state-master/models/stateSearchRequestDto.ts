import { ISearchRequestDto } from "../../../models/request/searchRequestDto";

export interface IStateSearchRequestDto extends ISearchRequestDto {
    name: string | null;
    countryName: string | null;
    pageIndex: number | null;
    pageSize: number | null;
    orderBy: string | null;
    orderByDirection: string | null;
}