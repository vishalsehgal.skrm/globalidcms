import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ICreateCountry } from '../../models/createcountry';
import { AddCountryService } from '../../services/add-country.service';
import { GetCountryListService } from '../../services/get-country-list.service';

const EMPTY_COUNTRY: ICreateCountry = {
  name: '',
  isoCode: '',
  isdCode: ''
};

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.scss'],
  providers:[
    AddCountryService,
    GetCountryListService
  ]
})
export class AddCountryComponent implements OnInit, OnDestroy {

  hasError: boolean;
  country: ICreateCountry;
  createCountryFormGroup: FormGroup;
  private subscriptions: Subscription[] = [];

  constructor(
    public addCountryService: AddCountryService,
    public getCountryListService: GetCountryListService,
    private fb: FormBuilder,
    public modal: NgbActiveModal
    
  ) { }

  ngOnInit(): void {
    this.country = EMPTY_COUNTRY;
    this.loadForm();
  }
 
  loadForm() {
    this.createCountryFormGroup = this.fb.group({
      name: ['',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z /-]{4,125}$')
        ])],
        isoCode: ['',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z]{2}$')
        ])],
      isdCode: ['',
        Validators.compose([
          Validators.required,
          Validators.pattern('^(\\+?\\d{1,3}|\\d(-)\\d{1,3})$')
        ])]
    });
  }

  async save() {
    this.prepareCountry();
    await this.create();
  }

  async create() {
    this.hasError = false;
   
   (await this.addCountryService.Create(this.country));
 
   this.addCountryService.isSuccess$.subscribe(x => 
    {
      if(x)
      {
        this.modal.close();
      }
      else
      {
        this.hasError= true;
      }
    });
  }

  private prepareCountry() {
    const formData = this.createCountryFormGroup.value;
    this.country.name = formData.name;
    this.country.isdCode = formData.isdCode;
    this.country.isoCode = formData.isoCode;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  // helpers for View
  isControlValid(name: string): boolean {
    const control = this.createCountryFormGroup.controls[name];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(name: string): boolean {
    const control = this.createCountryFormGroup.controls[name];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, name): boolean {
    const control = this.createCountryFormGroup.controls[name];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(name): boolean {
    const control = this.createCountryFormGroup.controls[name];
    return control.dirty || control.touched;
  }
}
