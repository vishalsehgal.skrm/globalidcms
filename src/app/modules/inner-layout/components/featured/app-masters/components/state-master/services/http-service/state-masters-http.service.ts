import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ApiResponse } from 'src/app/core/models/response/apiresponse';
import { IStateSearchRequestDto } from '../../models/stateSearchRequestDto';
import { IStateDto } from '../../models/stateResponseDto';
import { IPagination } from '../../../../models/response/pagination';
import { ICreateState } from '../../models/createState';
import { IUpdateState } from  '../../models/updateState';

@Injectable({
    providedIn: 'root',
})

export class StateMastersHTTPService {

    listUrl: string;
    detailsUrl: string;
    reviewUrl: string;
    statusUrl: string;
    deleteUrl: string;
    createUrl: string;
    updateUrl: string;

    constructor(private http: HttpClient) {
        
    }

    // public methods
    async GetStateListWithPagination(stateSearchRequestDto: IStateSearchRequestDto) : Promise<Observable<ApiResponse<IPagination<IStateDto>>>> {
        
        this.listUrl = `${environment.apiUrl}/api/v1/states`;

        let queryParams = new HttpParams();
        if (stateSearchRequestDto) {
            if (stateSearchRequestDto.name) {
                queryParams = queryParams.append("name", stateSearchRequestDto.name);
            }

            if(stateSearchRequestDto.countryName)
            {
                queryParams = queryParams.append("countryName", stateSearchRequestDto.countryName);
            }

             
            if(stateSearchRequestDto.orderBy)
            {
                queryParams = queryParams.append("orderBy", stateSearchRequestDto.orderBy);
            }

            if(stateSearchRequestDto.orderByDirection)
            {
                queryParams = queryParams.append("orderByDirection", stateSearchRequestDto.orderByDirection);
            }

            if(stateSearchRequestDto.pageIndex)
            {
                queryParams = queryParams.append("pageIndex", stateSearchRequestDto.pageIndex);
            }

            if(stateSearchRequestDto.pageSize)
            {
                queryParams = queryParams.append("pageSize", stateSearchRequestDto.pageSize);
            }
        }

        let result = await this.http.get<ApiResponse<IPagination<IStateDto>>>(this.listUrl, {params:queryParams});
        return result;
    }

    async UpdateReview(id: number) : Promise<Observable<ApiResponse<boolean>>> {
        this.reviewUrl = `${environment.apiUrl}/api/v1/states/${id}/review`;

        let result = await this.http.patch<ApiResponse<boolean>>(this.reviewUrl, null);
        return result;
    }

    async UpdateStatus(id: number) : Promise<Observable<ApiResponse<boolean>>> {
        this.statusUrl = `${environment.apiUrl}/api/v1/states/${id}/status`;

        let result = await this.http.patch<ApiResponse<boolean>>(this.statusUrl, null);
        return result;
    }

    async Delete(id: number) : Promise<Observable<ApiResponse<boolean>>> {
        this.deleteUrl = `${environment.apiUrl}/api/v1/states/${id}`;

        let result = await this.http.delete<ApiResponse<boolean>>(this.deleteUrl);
        return result;
    }

    async Create(state: ICreateState) : Promise<Observable<ApiResponse<boolean>>>
    {
        this.createUrl = `${environment.apiUrl}/api/v1/states`;

        let result = await this.http.post<ApiResponse<boolean>>(this.createUrl, state);
        return result;
    }

    async Update(state: IUpdateState) : Promise<Observable<ApiResponse<boolean>>>
    {
        this.updateUrl = `${environment.apiUrl}/api/v1/states`;

        let result = await this.http.put<ApiResponse<boolean>>(this.updateUrl, state);
        return result;
    }

    async GetDetails(id: number) : Promise<Observable<ApiResponse<IStateDto>>>
    {
        this.detailsUrl = `${environment.apiUrl}/api/v1/states/${id}/details`;

        let result = await this.http.get<ApiResponse<IStateDto>>(this.detailsUrl);
 
        return result;
    }
}
