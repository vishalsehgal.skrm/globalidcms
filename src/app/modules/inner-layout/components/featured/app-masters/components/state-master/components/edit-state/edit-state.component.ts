import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { catchError, map, of, Subscription } from 'rxjs';
import { IDropdownDto } from '../../../../models/response/dropdownDto';
import { GetCountryDropdownService } from '../../../country-master/services/get-country-dropdown.service';
import { ICreateState } from '../../models/createState';
import { UpdateStateService } from '../../services/update-state.service';
import { GetStateService } from '../../services/get-state-details.service';
import { IUpdateState } from '../../models/updateState';
import { IStateDto } from '../../models/stateResponseDto';

const EMPTY_STATE: IUpdateState = {
  id: 0,
  name: '',
  countryId: 0
};


@Component({
  selector: 'app-edit-state',
  templateUrl: './edit-state.component.html',
  styleUrls: ['./edit-state.component.scss'],
  providers:[
    UpdateStateService,
    GetStateService
  ]
})
export class EditStateComponent implements OnInit {

  @Input() id: number;
  
  isLoading: boolean;
  hasError: boolean;
  state: IUpdateState;
  createFormGroup: FormGroup;
  countryDropdownList: IDropdownDto[];
  stateDetails: IStateDto;

  private subscriptions: Subscription[] = [];

  constructor(
    public updateStateService: UpdateStateService,
    public getStateService: GetStateService,
    public getCountryDropdownService: GetCountryDropdownService,
    private fb: FormBuilder,
    public modal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    this.state = EMPTY_STATE;
    this.loadForm('', 0);
    this.loadCountryDropdown();
    this.loadState();

    const sb = this.getStateService.isLoading$.subscribe(res => this.isLoading = res);
    this.subscriptions.push(sb);
  }

  loadForm(name: string, countryId: number) {
    this.createFormGroup = this.fb.group({
      name: [name,
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z /-]{4,125}$')
        ])],
        countryId: [countryId,
        Validators.compose([
          Validators.required
        ])],
      otherName: ['']
    });
  }


  async loadState() {
    const sb = (await this.getStateService.GetDetails(this.id)).pipe(map(async (response: IStateDto) => {
      this.stateDetails = response;

      if (response) {
        this.state.id = response.id;
        this.state.name = response.name;
        this.state.countryId = response.countryId;

        await this.loadForm(this.state.name, this.state.countryId);
      }
    }),
      catchError((errorMessage) => {
        this.modal.dismiss(errorMessage);
        return of(EMPTY_STATE);
      })
    ).subscribe();

    this.subscriptions.push(sb);
  }


  async loadCountryDropdown() {
    const sb = (await this.getCountryDropdownService.GetCountryDropdown()).pipe(map(async (response: IDropdownDto[]) => {
      this.countryDropdownList = response;
    }) 
    ).subscribe();

    this.subscriptions.push(sb);
  }


  async save() {
    this.prepareCountry();
    await this.create();
  }

  async create() {
    this.hasError = false;
   
   (await this.updateStateService.Update(this.state));
 
   this.updateStateService.isSuccess$.subscribe(x => 
    {
      if(x)
      {
        this.modal.close();
      }
      else
      {
        this.hasError= true;
      }
    });
  }

  private prepareCountry() {
    const formData = this.createFormGroup.value;
    this.state.id = this.id;
    this.state.name = formData.name;
    this.state.countryId = formData.countryId;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  // helpers for View
  isControlValid(name: string): boolean {
    const control = this.createFormGroup.controls[name];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(name: string): boolean {
    const control = this.createFormGroup.controls[name];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, name): boolean {
    const control = this.createFormGroup.controls[name];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(name): boolean {
    const control = this.createFormGroup.controls[name];
    return control.dirty || control.touched;
  }
}
