import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { catchError, first, map, of, Subscription } from 'rxjs';
import { IUpdateCountry } from '../../models/updatecountry';
import { UpdateCountryService } from '../../services/update-country.service';
import { GetCountryService } from '../../services/get-country-details.service';
import { ICountryDto } from '../../models/countryResponseDto';

const EMPTY_COUNTRY: IUpdateCountry = {
  id: 0,
  name: '',
  isoCode: '',
  isdCode: ''
};

@Component({
  selector: 'app-edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.scss'],
  providers: [
    UpdateCountryService,
    GetCountryService
  ]
})
export class EditCountryComponent implements OnInit, OnDestroy {

  @Input() id: number;
  isLoading: boolean;
  hasError: boolean;
  country: IUpdateCountry;
  editFormGroup: FormGroup;
  private subscriptions: Subscription[] = [];

  constructor(
    public updateCountryService: UpdateCountryService,
    public getCountryService: GetCountryService,
    private fb: FormBuilder,
    public modal: NgbActiveModal
  ) { }

  async ngOnInit() {
    this.country = EMPTY_COUNTRY;
    await this.loadForm('', '', '');
    await this.loadCountry();

    const sb = this.getCountryService.isLoading$.subscribe(res => this.isLoading = res);
    this.subscriptions.push(sb);
  }

  countryDetails: ICountryDto;

  async loadCountry() {
    const sb = (await this.getCountryService.GetDetails(this.id)).pipe(map(async (response: ICountryDto) => {
      this.countryDetails = response;

      if (response) {
        this.country.id = response.id;
        this.country.name = response.name;
        this.country.isoCode = response.isoCode;
        this.country.isdCode = response.isdCode;

        await this.loadForm(this.country.name, this.country.isoCode, this.country.isdCode);
      }
    }),
      catchError((errorMessage) => {
        this.modal.dismiss(errorMessage);
        return of(EMPTY_COUNTRY);
      })
    ).subscribe();

    this.subscriptions.push(sb);
  }


  async loadForm(name: string, isoCode: string, isdCode: string) {

    this.editFormGroup = new FormGroup({

      name: new FormControl(name, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z /-]{4,125}$')
      ])),
      isoCode: new FormControl(isoCode, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]{2}$')
      ])),
      isdCode: new FormControl(isdCode, Validators.compose([
        Validators.required,
        Validators.pattern('^(\\+?\\d{1,3}|\\d(-)\\d{1,3})$')
      ]))

    });
  }

  async save() {
    this.prepareCountry();
    await this.update();
  }

  async update() {
    this.hasError = false;

    (await this.updateCountryService.Update(this.country));

    this.updateCountryService.isSuccess$.subscribe(x => {
      if (x) {
        this.modal.close();
      }
      else {
        this.hasError = true;
      }
    });
  }

  private prepareCountry() {
    const formData = this.editFormGroup.value;
    this.country.id = this.id;
    this.country.name = formData.name;
    this.country.isdCode = formData.isdCode;
    this.country.isoCode = formData.isoCode;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  // helpers for View
  isControlValid(controlName: string): boolean {
    const control = this.editFormGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.editFormGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.editFormGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.editFormGroup.controls[controlName];
    return control.dirty || control.touched;
  }
}
