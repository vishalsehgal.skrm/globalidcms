import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { catchError, map, Observable, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { IDropdownDto } from '../../../../models/response/dropdownDto';
import { GetCountryDropdownService } from '../../../country-master/services/get-country-dropdown.service';
import { ICreateState } from '../../models/createState';
import { AddStateService } from '../../services/add-state.service';
import { GetStateListService } from '../../services/get-state-list.service';

const EMPTY_STATE: ICreateState = {
  name: '',
  countryId: 0,
};

@Component({
  selector: 'app-add-state',
  templateUrl: './add-state.component.html',
  styleUrls: ['./add-state.component.scss'],
  providers:[
    AddStateService,
    GetStateListService
  ]
})
export class AddStateComponent implements OnInit {

  hasError: boolean;
  state: ICreateState;
  createFormGroup: FormGroup;
  countryDropdownList: IDropdownDto[];

  private subscriptions: Subscription[] = [];

  constructor(
    public addStateService: AddStateService,
    public getStateListService: GetStateListService,
    public getCountryDropdownService: GetCountryDropdownService,
    private fb: FormBuilder,
    public modal: NgbActiveModal
  ) { }


  ngOnInit(): void {
    this.state = EMPTY_STATE;
    this.loadForm();
    this.loadCountryDropdown();
  }

  loadForm() {
    this.createFormGroup = this.fb.group({
      name: ['',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z /-]{4,125}$')
        ])],
        countryId: ['',
        Validators.compose([
          Validators.required
        ])] 
      
    });
  }


  async loadCountryDropdown() {
    const sb = (await this.getCountryDropdownService.GetCountryDropdown()).pipe(map(async (response: IDropdownDto[]) => {
      this.countryDropdownList = response;
    }) 
    ).subscribe();

    this.subscriptions.push(sb);
  }


  async save() {
    this.prepareCountry();
    await this.create();
  }

  async create() {
    this.hasError = false;
   
   (await this.addStateService.Create(this.state));
 
   this.addStateService.isSuccess$.subscribe(x => 
    {
      if(x)
      {
        this.modal.close();
      }
      else
      {
        this.hasError= true;
      }
    });
  }

  private prepareCountry() {
    const formData = this.createFormGroup.value;
    this.state.name = formData.name;
    this.state.countryId = formData.countryId;
  
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  // helpers for View
  isControlValid(name: string): boolean {
    const control = this.createFormGroup.controls[name];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(name: string): boolean {
    const control = this.createFormGroup.controls[name];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, name): boolean {
    const control = this.createFormGroup.controls[name];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(name): boolean {
    const control = this.createFormGroup.controls[name];
    return control.dirty || control.touched;
  }
}
