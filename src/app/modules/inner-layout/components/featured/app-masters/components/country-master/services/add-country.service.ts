import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/core/models/response/apiresponse';
import { BehaviorSubject, observable, Observable, of, Subscription } from 'rxjs';
import { catchError, finalize, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CountryMastersHTTPService } from './http-service/country-masters-http.service';
import { ICreateCountry } from '../models/createcountry';
 
@Injectable({ providedIn: 'root' })

export class AddCountryService {

  // Private fields
  private _isAddCountryLoading$ = new BehaviorSubject<boolean>(false);  
  private _errorMessage$ = new BehaviorSubject<string>('');
  private _successMessage$ = new BehaviorSubject<string>('');
  private _isSuccess$ = new BehaviorSubject<boolean>(false);
  private _subscriptions$: Subscription[] = [];

  //Observables
  get isLoading$() {
    return this._isAddCountryLoading$.asObservable();
  }

  get errorMessage$() {
    return this._errorMessage$.asObservable();
  }

  get successMessage$() {
    return this._successMessage$.asObservable();
  }

  get isSuccess$() {
    return this._isSuccess$.asObservable();
  }

  get subscriptions() {
    return this._subscriptions$;
  }

  //Observable Values
  get isLoadingValue$() {
    return this._isAddCountryLoading$.value;
  }

  get errorMessageValue$() {
    return this._errorMessage$.value;
  }

  get successMessageValue$() {
    return this._successMessage$.value;
  }

  get isSuccessValue$() {
    return this._isSuccess$.value;
  }
 
  constructor(
    private router: Router,
    private appMastersHTTPService: CountryMastersHTTPService
  ) {

  }
 

  async Create(country: ICreateCountry) {

    this._isAddCountryLoading$.next(true);
    this._errorMessage$.next('');
    this._successMessage$.next('');
    this._isSuccess$.next(false);
 
    const request = (await this.appMastersHTTPService.Create(country))
      .pipe(
        tap(
          (response: ApiResponse<boolean>) => {
 
            if (response) {
              this._isSuccess$.next(response.success);

              if (response.success) {
                  this._successMessage$.next(response.message);
              }
              else {
                this._errorMessage$.next(response.message);
              }

              
            }
            else {
              this._isSuccess$.next(false);
              this._errorMessage$.next('Something went wrong. Please try again later.')
              
            }
          }
        ),
        catchError((err) : any => {
          this._isSuccess$.next(false);

          console.log(err);
          this._errorMessage$.next(err.error.message);
          
        }),
        finalize(() => {
          this._isAddCountryLoading$.next(false);
        })
      )
      .subscribe();

    this._subscriptions$.push(request);
  }
}