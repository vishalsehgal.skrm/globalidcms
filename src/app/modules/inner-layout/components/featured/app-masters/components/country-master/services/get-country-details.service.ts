import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/core/models/response/apiresponse';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { catchError, finalize, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CountryMastersHTTPService } from './http-service/country-masters-http.service';
import { ICountryDto } from '../models/countryResponseDto';

@Injectable({ providedIn: 'root' })

export class GetCountryService {

  // Private fields
  private _itemDetails$ = new BehaviorSubject<ICountryDto>(null);

  private _isLoading$ = new BehaviorSubject<boolean>(false);
  private _errorMessage$ = new BehaviorSubject<string>('');
  private _successMessage$ = new BehaviorSubject<string>('');
  private _isSuccess$ = new BehaviorSubject<boolean>(false);
  private _subscriptions$: Subscription[] = [];

  //Observables
  get itemDetails$() {
    return this._itemDetails$.asObservable();
  }

  get isLoading$() {
    return this._isLoading$.asObservable();
  }

  get errorMessage$() {
    return this._errorMessage$.asObservable();
  }

  get successMessage$() {
    return this._successMessage$.asObservable();
  }

  get isSuccess$() {
    return this._isSuccess$.asObservable();
  }

  get subscriptions() {
    return this._subscriptions$;
  }

  //Observable Values
  get isLoadingValue$() {
    return this._isLoading$.value;
  }

  get errorMessageValue$() {
    return this._errorMessage$.value;
  }

  get successMessageValue$() {
    return this._successMessage$.value;
  }

  get isSuccessValue$() {
    return this._isSuccess$.value;
  }

  get itemDetailsValue$() {
    return this._itemDetails$.value;
  }
 

  constructor(
    private router: Router,
    private appMastersHTTPService: CountryMastersHTTPService
  ) {

  }

  async GetDetails(id: number) : Promise<Observable<ICountryDto>>{

    this._isLoading$.next(true);
    this._errorMessage$.next('');
    this._successMessage$.next('');
    this._isSuccess$.next(false);

    const request = (await this.appMastersHTTPService.GetDetails(id))
      .pipe(
        map(
          (response: ApiResponse<ICountryDto>) => {

            if (response) {
              if (response.success) {
                if (response.data) {
                  this._itemDetails$.next(response.data);
                  this._isSuccess$.next(true);
                }
                else {
                  this._isSuccess$.next(false);
                  this._errorMessage$.next(response.message);
                }
              }
              else {
                this._isSuccess$.next(false);
                this._errorMessage$.next(response.message);
              }
            }
            else {
              this._isSuccess$.next(false);
              this._errorMessage$.next('Something went wrong. Please try again later.')
            }
          }
        ),
        catchError((err): any => {
          this._isSuccess$.next(false);
          this._errorMessage$.next(err.error.message);
          return of(null);
        }),
        finalize(() => {
          this._isLoading$.next(false);
        })
      )
      .subscribe();

    this._subscriptions$.push(request);

    return this._itemDetails$;
  }


}