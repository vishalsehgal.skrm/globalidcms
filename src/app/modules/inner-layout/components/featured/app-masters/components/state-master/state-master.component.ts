import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { IStateSearchRequestDto } from './models/stateSearchRequestDto';
import { IStateDto } from './models/stateResponseDto';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortState, ISortView } from 'src/app/modules/sort-icon/sort.model';
import { DatePipe } from '@angular/common';

import { AddStateComponent } from './components/add-state/add-state.component';
import { EditStateComponent } from './components/edit-state/edit-state.component';

import { GetStateListService } from './services/get-state-list.service';
import { UpdateStateReviewService } from './services/update-state-review.service';
import { UpdateStateStatusService } from './services/update-state-status.service';
import { DeleteStateService } from './services/delete-state.service';

 
import Swal from 'sweetalert2';

const EMPTY_STATE_SEARCH: IStateSearchRequestDto = {
  orderBy: 'Id',
  orderByDirection: 'Desc',
  name: '',
  countryName: '',
  pageIndex: 0,
  pageSize: 10
};


@Component({
  selector: 'app-state-master',
  templateUrl: './state-master.component.html',
  styleUrls: ['./state-master.component.scss'],
  providers: [
    GetStateListService,
    UpdateStateReviewService,
    UpdateStateStatusService,
    DeleteStateService
  ]
})
export class StateMasterComponent implements OnInit, ISortView {
 
  searchform: FormGroup;
  pageIndex: number;
  pageSize: number;
  totalRecordCount: number;

  sorting: SortState;
  isLoading: boolean;
  hasError: boolean;
  errorMessage: string = '';
  stateSearchRequestDto: IStateSearchRequestDto;

  // private fields
  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

  stateList: IStateDto[];
  private subscriptions: Subscription[] = [];

  constructor(
    private datePipe: DatePipe,
    public getStateListService: GetStateListService,
    public updateStateReviewService: UpdateStateReviewService,
    public updateStateStatusService: UpdateStateStatusService,
    public deleteStateService: DeleteStateService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder

  ) {
  }


  async ngOnInit() {

    this.stateSearchRequestDto = EMPTY_STATE_SEARCH;
    this.loadForm();

    this.sorting = new SortState();
    this.sorting.column = this.getStateListService.sortingColumnName$;
    this.sorting.direction = this.getStateListService.sortingDirection$;

    await this.getStateListService.getList(null);
    this.getStateListService.pageIndex$.subscribe(x => this.pageIndex = x);
    this.getStateListService.pageSize$.subscribe(x => this.pageSize = x);
    this.getStateListService.totalItems$.subscribe(x => this.totalRecordCount = x);

    const sb = this.getStateListService.isLoading$.subscribe(res => this.isLoading = res);
    this.subscriptions.push(sb);
  }

  loadForm() {
    this.searchform = this.fb.group({
      searchName: [''],
      searchCountryName: ['']
    });
  }

  private prepareState() {
    const formData = this.searchform.value;
    this.stateSearchRequestDto.name = formData.searchName;
    this.stateSearchRequestDto.countryName = formData.searchCountryName;
    this.stateSearchRequestDto.pageIndex = this.pageIndex;

    this.stateSearchRequestDto.orderBy = this.sorting.column;
    this.stateSearchRequestDto.orderByDirection = this.sorting.direction;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }

  // sorting
  async sort(column: any) {
    const sorting = this.sorting;

    const isActiveColumn = sorting.column === column;
    if (!isActiveColumn) {
      sorting.column = column;
      sorting.direction = 'asc';
    } else {
      sorting.direction = sorting.direction === 'asc' ? 'desc' : 'asc';
    }

    this.prepareState();

    this.stateSearchRequestDto.orderBy = this.sorting.column;
    this.stateSearchRequestDto.orderByDirection = this.sorting.direction;

    await this.getStateListService.getList(this.stateSearchRequestDto);
  }

  async create() {
    const modalRef = this.modalService.open(AddStateComponent, { size: 'md' });
    modalRef.result.then(() =>
    this.successNotification('State added successfully!')
    );
  }

  async edit(id: number) {

    const modalRef = this.modalService.open(EditStateComponent, { size: 'md' });
    modalRef.componentInstance.id = id;
    modalRef.result.then(() =>
    this.successNotification('State updated successfully!')
    );
  }

  async search() {

    this.prepareState();
    await this.getStateListService.getList(this.stateSearchRequestDto);
  }

  async resetSearch() {
    this.pageIndex = 1;
    this.searchform.reset();
    this.sorting.column = 'Id';
    this.sorting.direction = 'Desc';
    await this.search();
  }


  async review(event: any) {
    const sb = this.updateStateReviewService.isLoading$.subscribe(res => this.isLoading = res);

    await this.updateStateReviewService.UpdateReview(event);

    this.subscriptions.push(sb);
  }

  async status(event: any) {
    const sb = this.updateStateStatusService.isLoading$.subscribe(res => this.isLoading = res);

    await this.updateStateStatusService.UpdateStatus(event);

    this.subscriptions.push(sb);
  }

  async delete(event: any) {
    const sb = this.deleteStateService.isLoading$.subscribe(res => this.isLoading = res);

    await this.deleteStateService.Delete(event);

    this.subscriptions.push(sb);
  }

  async onPageChanged(event: any) {

    this.pageIndex = event;
    this.prepareState();
    
    this.stateSearchRequestDto.pageIndex = event;

    if (event <= 1) {
      this.stateSearchRequestDto.orderBy = 'Id';
      this.stateSearchRequestDto.orderByDirection = 'Desc';
    }
    else {
      this.stateSearchRequestDto.orderBy = this.sorting.column;
      this.stateSearchRequestDto.orderByDirection = this.sorting.direction;
    }

    await this.getStateListService.getList(this.stateSearchRequestDto);

  }

  successNotification(message: string) {
    Swal.fire({
      title: 'Success!',
      text: message,
      icon: 'success'
    }).then(async (result) => {
      await this.search();
    });
  }

  deleteConfirmation(id: any) {
    Swal.fire({
      title: 'Are you sure to delete?',
      text: 'This process is irreversible.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, go ahead.',
      cancelButtonText: 'No, let me think',
    }).then(async (result) => {
      if (result.value) {

        await this.delete(id);

        Swal.fire('Removed!', 'State removed successfully.', 'success').then(async () => {
          await this.search();
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'State still in our database.', 'error');
      }
    });
  }

  statusConfirmation(id: any, status: any) {
    Swal.fire({
      title: status ? 'Are you sure to deactivate?' : 'Are you sure to activate?',
      text: status ? 'This process will deactivate the record.' : 'This process will activate the record.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, go ahead.',
      cancelButtonText: 'No, let me think',
    }).then(async (result) => {
      if (result.value) {

        await this.status(id);

        Swal.fire(status ? 'Deactive!' : 'Activate!', status ? 'State deactivated successfully.' : 'State activated successfully.', 'success').then(async () => {
          await this.search();
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', status ? 'State still activated.' : 'State still deactivated.', 'error');
      }
    });
  }

  reviewConfirmation(id: any) {
    Swal.fire({
      title: 'Are you sure to review?',
      text: 'This process will mark the record as "Reviewed".',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, go ahead.',
      cancelButtonText: 'No, let me think',
    }).then(async (result) => {
      if (result.value) {

        await this.review(id);

        Swal.fire('Reviewed!', 'State reviewed successfully.', 'success').then(async () => {
          await this.search();
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'State review still pending.', 'error');
      }
    });
  }
}
