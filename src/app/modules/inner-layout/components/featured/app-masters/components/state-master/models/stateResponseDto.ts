export interface IStateDto{
        id: number;
        name: string;
        countryId: number;
        countryName: string;
        createdBy: number;
        createdByName: string;
        createdDate: Date;
        modifiedBy: number;
        modifiedByName: string;
        modifiedDate: Date;
        isActive: boolean;
        isReviewed: boolean;
        reviewedBy: number;
        reviewedByName: string;
        reviewedDate: Date;
        isDeleted: boolean;
        deletedDate: Date;
}