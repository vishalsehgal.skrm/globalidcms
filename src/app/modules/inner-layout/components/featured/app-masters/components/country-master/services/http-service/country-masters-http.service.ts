import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ApiResponse } from 'src/app/core/models/response/apiresponse';
import { ICountrySearchRequestDto } from '../../models/countrySearchRequestDto';
import { ICountryDto } from '../../models/countryResponseDto';
import { IPagination } from '../../../../models/response/pagination';
import { ICreateCountry } from '../../models/createcountry';
import { IUpdateCountry } from  '../../models/updatecountry';
import { IDropdownDto } from '../../../../models/response/dropdownDto';

@Injectable({
    providedIn: 'root',
})

export class CountryMastersHTTPService {

    listUrl: string;
    detailsUrl: string;
    reviewUrl: string;
    statusUrl: string;
    deleteUrl: string;
    createUrl: string;
    updateUrl: string;

    constructor(private http: HttpClient) {
        
    }

    // public methods
    async GetCountryListWithPagination(countrySearchRequestDto: ICountrySearchRequestDto) : Promise<Observable<ApiResponse<IPagination<ICountryDto>>>> {
        
        this.listUrl = `${environment.apiUrl}/api/v1/country`;

        let queryParams = new HttpParams();
        if (countrySearchRequestDto) {
            if (countrySearchRequestDto.name) {
                queryParams = queryParams.append("name", countrySearchRequestDto.name);
            }

            if(countrySearchRequestDto.isoCode)
            {
                queryParams = queryParams.append("isoCode", countrySearchRequestDto.isoCode);
            }

            if(countrySearchRequestDto.isdCode)
            {
                queryParams = queryParams.append("isdCode", countrySearchRequestDto.isdCode);
            }

            if(countrySearchRequestDto.orderBy)
            {
                queryParams = queryParams.append("orderBy", countrySearchRequestDto.orderBy);
            }

            if(countrySearchRequestDto.orderByDirection)
            {
                queryParams = queryParams.append("orderByDirection", countrySearchRequestDto.orderByDirection);
            }

            if(countrySearchRequestDto.pageIndex)
            {
                queryParams = queryParams.append("pageIndex", countrySearchRequestDto.pageIndex);
            }

            if(countrySearchRequestDto.pageSize)
            {
                queryParams = queryParams.append("pageSize", countrySearchRequestDto.pageSize);
            }
        }

        let result = await this.http.get<ApiResponse<IPagination<ICountryDto>>>(this.listUrl, {params:queryParams});
        return result;
    }

    async UpdateReview(id: number) : Promise<Observable<ApiResponse<boolean>>> {
        this.reviewUrl = `${environment.apiUrl}/api/v1/country/${id}/review`;

        let result = await this.http.patch<ApiResponse<boolean>>(this.reviewUrl, null);
        return result;
    }

    async UpdateStatus(id: number) : Promise<Observable<ApiResponse<boolean>>> {
        this.statusUrl = `${environment.apiUrl}/api/v1/country/${id}/status`;

        let result = await this.http.patch<ApiResponse<boolean>>(this.statusUrl, null);
        return result;
    }

    async Delete(id: number) : Promise<Observable<ApiResponse<boolean>>> {
        this.deleteUrl = `${environment.apiUrl}/api/v1/country/${id}`;

        let result = await this.http.delete<ApiResponse<boolean>>(this.deleteUrl);
        return result;
    }

    async Create(country: ICreateCountry) : Promise<Observable<ApiResponse<boolean>>>
    {
        this.createUrl = `${environment.apiUrl}/api/v1/country`;

        let result = await this.http.post<ApiResponse<boolean>>(this.createUrl, country);
        return result;
    }

    async Update(country: IUpdateCountry) : Promise<Observable<ApiResponse<boolean>>>
    {
        this.updateUrl = `${environment.apiUrl}/api/v1/country`;

        let result = await this.http.put<ApiResponse<boolean>>(this.updateUrl, country);
        return result;
    }

    async GetDetails(id: number) : Promise<Observable<ApiResponse<ICountryDto>>>
    {
        this.detailsUrl = `${environment.apiUrl}/api/v1/country/${id}/details`;

        let result = await this.http.get<ApiResponse<ICountryDto>>(this.detailsUrl);
 
        return result;
    }

    async GetCountryDropdown() : Promise<Observable<ApiResponse<IDropdownDto[]>>>
    {
        this.detailsUrl = `${environment.apiUrl}/api/v1/country/dropdown`;

        let result = await this.http.get<ApiResponse<IDropdownDto[]>>(this.detailsUrl);
 
        return result;
    }

}
