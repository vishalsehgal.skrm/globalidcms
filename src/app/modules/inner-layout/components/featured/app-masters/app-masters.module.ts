import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppMastersComponent } from './app-masters.component';
import { NgbModalModule, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SortIconModule } from 'src/app/modules/sort-icon/sort-icon.module';

import { AppMastersRoutingModule } from './app-masters-routing.module';

import { CountryMasterComponent } from './components/country-master/country-master.component';
import { AddCountryComponent } from './components/country-master/components/add-country/add-country.component';
import { EditCountryComponent } from './components/country-master/components/edit-country/edit-country.component';

import { StateMasterComponent } from './components/state-master/state-master.component';
import { AddStateComponent } from './components/state-master/components/add-state/add-state.component';
import { EditStateComponent } from './components/state-master/components/edit-state/edit-state.component';
 
@NgModule({
  declarations: [
    AppMastersComponent,

    CountryMasterComponent,
    AddCountryComponent,
    EditCountryComponent,

    StateMasterComponent,
    AddStateComponent,
    EditStateComponent

  ],
  imports: [
    CommonModule,
    AppMastersRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModalModule,
    SortIconModule,
    NgbPaginationModule,
    NgbTypeaheadModule
  ],
  providers: [

  ]
})
export class AppMastersModule { }
