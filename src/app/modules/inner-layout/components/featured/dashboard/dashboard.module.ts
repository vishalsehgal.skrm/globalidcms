import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { DashCountersComponent } from './dash-counters/dash-counters.component';
import { AuthGuard } from 'src/app/core/guard/auth.guard';

@NgModule({
  declarations: [DashboardComponent, DashCountersComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        canActivate: [AuthGuard],
        component: DashboardComponent,
      },
    ])
  ],
})
export class DashboardModule {}
