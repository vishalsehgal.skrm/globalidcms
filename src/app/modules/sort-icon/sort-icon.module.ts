import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { SortIconComponent } from "./sort-icon.component";
import { InlineSVGModule } from 'ng-inline-svg-2';

@NgModule({
    declarations: [SortIconComponent],
    imports: [CommonModule, FormsModule, InlineSVGModule],
    exports: [SortIconComponent]
  })
  export class SortIconModule {
  }