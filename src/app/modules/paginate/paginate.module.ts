import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { InlineSVGModule } from 'ng-inline-svg-2';
import { PaginateComponent } from "./paginate.component";
import { NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap'

@NgModule({
    declarations: [PaginateComponent],
    imports: [CommonModule, 
        FormsModule, 
        InlineSVGModule,
        NgbPaginationModule
    ],
    exports: [PaginateComponent, NgbPaginationModule]
  })
  export class PaginateModule {
  }