import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Routing } from './route-configuration/routing';

@NgModule({
  imports: [RouterModule.forRoot(Routing)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
