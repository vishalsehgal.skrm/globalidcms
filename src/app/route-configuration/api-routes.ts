export class APIRoutes {
    //AUTH
    static login: string = 'api/v1/auth';
    static logout: string = 'api/v1/auth/logout';

    //REFRESH TOKEN
    static refreshToken: string = 'api/v1/refreshtoken';

    //COUNTRY
    static countryList: string = 'api/v1/country';
    static addCountry: string = 'api/v1/country';
}