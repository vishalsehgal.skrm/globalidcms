import { Routes } from '@angular/router';
import { AuthGuard } from '../core/guard/auth.guard';

const authModule = () => import('../modules/auth/auth.module').then(x => x.AuthModule);
const errorModule = () => import('../modules/errors/errors.module').then(x => x.ErrorsModule);
const innerLayoutModule = () => import('../modules/inner-layout/inner-layout.module').then(x => x.InnerLayoutModule);

const Routing: Routes = [

    { path: 'auth', loadChildren: authModule },
    { path: 'error', loadChildren: errorModule },
    { path: '', 
    canActivate: [AuthGuard], 
    loadChildren: innerLayoutModule },

    // otherwise redirect to home
    { path: '**', redirectTo: '/auth/login' }
];
export { Routing };
