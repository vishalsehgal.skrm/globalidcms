import { Routes } from '@angular/router';
import { AuthGuard } from '../core/guard/auth.guard';

const InnerLayoutRouting: Routes = [
    {
        path: 'dashboard',
        canActivate: [AuthGuard],
        loadChildren: () =>
            import('src/app/modules/inner-layout/components/featured/dashboard/dashboard.module').then((m) => m.DashboardModule),
    },
    {
        path: 'masters',
        canActivate: [AuthGuard],
        loadChildren: () =>
            import('src/app/modules/inner-layout/components/featured/app-masters/app-masters.module').then((m) => m.AppMastersModule),
    },

    {
        path: '',
        canActivate: [AuthGuard],
        redirectTo: '/dashboard',
        pathMatch: 'full',
    },
    {
        path: '**',
        canActivate: [AuthGuard],
        redirectTo: '',
    },
];

export { InnerLayoutRouting };
