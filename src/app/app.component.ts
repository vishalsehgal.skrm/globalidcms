import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {TranslationService} from './modules/i18n';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { SplashScreenService } from './modules/partials/layout/splash-screen/splash-screen.service';
 
@Component({
  selector: 'body[root]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, OnDestroy  {
  private unsubscribe: Subscription[] = [];

  constructor(
    
    private splashScreenService: SplashScreenService,
    private router: Router
  ) {
     
    
  }

  ngOnInit() {
     
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

}
